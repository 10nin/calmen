(defproject calmen "0.1.0-SNAPSHOT"
  :description "A Tokyo Metropolitan Library annual calender getter."
  :url ""
  :license {:name "MIT License"
            :url "https://opensource.org/licenses/MIT"}
  :dependencies [[org.clojure/clojure "1.8.0"]
                 [enlive "1.1.6"]
                 [selmer "1.11.7"]])
